## Solution

The solution uses

- Java 8
- Spring Boot and Spring MVC frameworks
- Apache Maven for build and test execution
- Optional: VirtualBox for build, test and runtime environment
- Optional: Vagrant for Virtual Machine build and configuration

---

## Current pre-requisits  

Build on native OS

1. Java 8 - for compiling and running
2. Git Client - to checkout code from bitbucket.  Examples: git for Windows or SourceTree

or to create VM for build

1. VirtualBox - H/W Vitualisation
2. Vagrant - VM build automation
3. Git Client - to checkout code from bitbucket

---

## To Create VM as build environment

To create a build environment a VagrantFile exists in the root of the project.  This can be used to build a virtual linux machine which can then be used build and runtime environment.

1. Install VirtualBox v5.2.x from https://www.virtualbox.org/
2. Install Vagrant v2.1.x from https://www.vagrantup.com/
3. Clone the the project from https://bitbucket.org/philmyster99/test/src/master/
3. Run 'vagrant up' from the local root of the git repo cloned
4. Connect to the VM created 'vagrant ssh'
5. Follow 'To Build' steps below.

---

## To Build

1. Clone the the project from https://bitbucket.org/philmyster99/test/src/master/
2. Build the project './mvnw clean package' from the root of the git repo cloned
3. Run to application 'java -jar target/test-1.0.0-SNAPSHOT.jar'
4. From a browser goto http://localhost:8080/details

---

## Work Outstanding

1. Use Spring Test framework for unit testing Controller (Partial)
2. Improve assertion on model to validate functionality
3. Create Docker image for running application (simplify deployment and running)
4. Redirect / to /details for usability
5. Validation on Bank Name and Card Number
6. Invalid CSV file checks
7. Add error page mapping
8. Try using different JS frameworks for UI rather than thymeleaf templating, for example AngularJS, ReactJS, Vue.JS, ....
