package com.example.test.model;

import java.time.Month;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

//@Component
//@Scope(proxyMode=ScopedProxyMode.TARGET_CLASS, value="session")
public class BankCardDetails {
    private List<BankCard> details;

    public BankCardDetails()
    {
        details = new ArrayList<BankCard>();
    }

    public void addBankCard(BankCard bankCard)
    {
        details.add(bankCard);
    }

    public BankCard[] getBankCardDetails()
    {
        BankCard[] cards = details.toArray(new BankCard[0]);

        /* NOTE: '-' in the lambda expression to reverse sort order */
        Arrays.sort(cards, (bankCard, t1) -> -bankCard.getExpiryDate().compareTo(t1.getExpiryDate()));

        return cards;
    }

    public int numberOfBankCards()
    {
        return details.size();
    }

    public void loadBankCards(String csvContent)
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM-yyyy");
        details.clear();

        String lines[] = csvContent.split("\n");

        for (String line : lines) {
            String fields[] = line.split(",");
            YearMonth expiry = YearMonth.parse(fields[2].trim(),formatter);
            BankCard card = new BankCard(fields[0].trim(), fields[1].trim(), expiry.getMonth(), expiry.getYear());
            details.add(card);
        }
    }
}

