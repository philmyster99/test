package com.example.test.model;

import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.YearMonth;

public class BankCard {
    private String bankName;
    private String cardNumber;
    private YearMonth expiryDate;

    public BankCard(String bankName, String cardNumber, Month expiryMonth, int expiryYear) {
        this.bankName = bankName;
        this.cardNumber = cardNumber;
        this.expiryDate = YearMonth.of(expiryYear,expiryMonth);
    }

    public String getBankName() {
        return this.bankName;
    }

    public String getCardNumber() {
        String number = "";
        if (cardNumber.length() < 4) {
            number = this.cardNumber.substring(0,cardNumber.length());
        }
        else {
            number = this.cardNumber.substring(0,4).concat("-xxxx-xxxx-xxxx");
        }
        return number;
    }

    public YearMonth getExpiryDate()
    {
        return this.expiryDate;
    }


}
