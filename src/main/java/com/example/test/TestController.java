package com.example.test;

import com.example.test.model.BankCard;
import com.example.test.model.BankCardDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.Month;

@SessionAttributes("details")
@Controller
public class TestController
{
    private final static int NUMBER_OF_YEARS = 6;

    private final static String MESSAGE = "errors";

    @RequestMapping(path = "/details")
    public String details(@ModelAttribute("details") BankCardDetails details,Model model)
    {
        model.addAttribute("bankcards", details.getBankCardDetails());
        return "viewDetails";
    }

    @PostMapping(path = "/add")
    public String add(@ModelAttribute("details") BankCardDetails details,
                      @RequestParam("bankName") String bankName,
                      @RequestParam("cardNumber") String cardNumber,
                      @RequestParam("expiryMonth") String expiryMonth,
                      @RequestParam("expiryYear") String expiryYear,
                      Model model)
    {
        int year = Integer.parseInt(expiryYear);
        Month month = Month.valueOf(expiryMonth);

        if (cardNumber.length()<16) {
            model.addAttribute(MESSAGE, "Invalid card number entered - Must be a full 16 digits");
        }
        else {
            BankCard bankCard = new BankCard(bankName, cardNumber, month, year);
            details.addBankCard(bankCard);
        }
        return "forward:details";
    }

    private String convertMultipartFileToString(MultipartFile file) throws IOException
    {
        StringBuilder content = new StringBuilder();
        int bytesRead;
        byte[] fileContent = new byte[4096];
        InputStream is = file.getInputStream();
        while ((bytesRead = is.read(fileContent))>0)
        {
            String s = new String(fileContent,0,bytesRead,StandardCharsets.UTF_8);
            content.append(s);
        }
        return content.toString();
    }

    @PostMapping("/upload")
    public String upload(@ModelAttribute("details") BankCardDetails details, @RequestParam("file") MultipartFile file, Model model)
    {
        if (file.isEmpty()) {
            model.addAttribute(MESSAGE, "Failed to upload file or file is empty");
            return "forward:details";
        }

        try {
            String content = convertMultipartFileToString(file);
            details.loadBankCards(content);
        }
        catch (IOException e) {
            model.addAttribute(MESSAGE, "Failed to read file");
        }

        return "forward:details";
    }

    @ModelAttribute("details")
    public BankCardDetails getDetails()
    {
        return new BankCardDetails();
    }

    @ModelAttribute("months")
    public Month[] getMonths()
    {
        return Month.values();
    }

    @ModelAttribute("years")
    public String[] getYears()
    {
        String[] years = new String[NUMBER_OF_YEARS];
        int year = LocalDate.now().getYear();
        for (int i = 0; i < NUMBER_OF_YEARS; i++) {
            years[i] = Integer.toString(year + i);
        }
        return years;
    }
}
