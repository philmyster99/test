package com.example.test.model;

import com.example.test.model.BankCard;
import com.example.test.model.BankCardDetails;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;

public class TestBankCardDetails {
    private final static String BANK_NAME = "HBOS";
    private final static String CARD_NUMBER = "7684-4564-3456-1234";
    private final static String EXPIRY_DATE = "Feb-2019";

    BankCardDetails details;

    @Before
    public void setup()
    {
        details = new BankCardDetails();
    }

    @Test
    public void shouldLoadOneCard()
    {
        String content = BANK_NAME + "," + CARD_NUMBER + "," + EXPIRY_DATE;
        details.loadBankCards(content);
        BankCard[] cards = details.getBankCardDetails();
        Assert.assertEquals(cards.length,1);
        Assert.assertEquals(BANK_NAME, cards[0].getBankName());
        Assert.assertEquals(CARD_NUMBER.substring(0,4), cards[0].getCardNumber().substring(0,4));
        Assert.assertEquals(YearMonth.of(2019,Month.FEBRUARY), cards[0].getExpiryDate());
    }

    @Test
    public void shouldReturnCardInDecendingExpiryDateOrder()
    {
        details.addBankCard(new BankCard("A", "1", Month.JANUARY, 2018));
        details.addBankCard(new BankCard("A", "1", Month.DECEMBER, 2019));
        details.addBankCard(new BankCard("A", "1", Month.MARCH, 2020));

        BankCard[] cards = details.getBankCardDetails();

        YearMonth lastCardExpiryDate = null;
        for (BankCard card : cards) {
            if (lastCardExpiryDate != null) {
                System.out.println("Comparing "+lastCardExpiryDate+" and "+card.getExpiryDate());
                Assert.assertTrue(lastCardExpiryDate.isAfter(card.getExpiryDate()));
            }
            lastCardExpiryDate = card.getExpiryDate();
        }
    }
}
