package com.example.test.model;

import com.example.test.model.BankCard;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.Month;

public class TestBankCard {
    private static final String CARD_NUMBER  = "1234-5678-9012-3456";
    private static final String BANK_NAME = "Some Bank";
    private static final Month EXPIRY_MONTH = Month.APRIL;
    private static final int EXPIRY_YEAR = 2020;

    private BankCard card, cardWithBlankNumber;

    @Before
    public void setup()
    {
        card = new BankCard(BANK_NAME, CARD_NUMBER, EXPIRY_MONTH, EXPIRY_YEAR);
        cardWithBlankNumber = new BankCard(BANK_NAME, "", EXPIRY_MONTH, EXPIRY_YEAR);
    }

    @Test
    public void shouldNotSeeFullCardNumber()
    {
        Assert.assertNotSame(CARD_NUMBER, card.getCardNumber());
        Assert.assertEquals(CARD_NUMBER.substring(0,4), card.getCardNumber().subSequence(0,4));
    }

    @Test
    public void shouldGetCorrectExpiryDate()
    {
        Assert.assertEquals(EXPIRY_YEAR, card.getExpiryDate().getYear());
        Assert.assertEquals(EXPIRY_MONTH, card.getExpiryDate().getMonth());
    }

    @Test
    public void shouldGetBlankCardNumber()
    {
        Assert.assertEquals("", cardWithBlankNumber.getCardNumber());
    }
}
