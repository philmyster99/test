package com.example.test;

import com.example.test.model.BankCard;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.multipart.MultipartFile;

import java.time.Month;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = TestController.class)
public class TestControllerTests {
    private static final String BANK_NAME = "some bank";
    private static final String CARD_NUMBER = "1234-5678-9012-3456";
    private static final Month EXPIRY_MONTH = Month.JANUARY;
    private static final int EXPIRY_YEAR = 2018;

    private static final BankCard card = new BankCard(BANK_NAME, CARD_NUMBER, EXPIRY_MONTH, EXPIRY_YEAR);
    private static final Month[] months = {
            Month.JANUARY, Month.FEBRUARY, Month.MARCH, Month.APRIL,
            Month.MAY, Month.JUNE, Month.JULY, Month.AUGUST,
            Month.SEPTEMBER, Month.OCTOBER, Month.NOVEMBER, Month.DECEMBER
    };

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private MultipartFile mockFile;

    @Test
    public void shouldReturnDefaultDetailsPage() throws Exception {
        mockMvc.perform(get("/details"))
                .andExpect(status().isOk())
                .andExpect(view().name("viewDetails"))
                .andExpect(model().attribute("months",months));
    }

    @Test
    public void shouldAddBankCardDetails() throws Exception
    {
        mockMvc.perform(post("/add")
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .param("bankName", BANK_NAME)
                    .param("cardNumber", CARD_NUMBER)
                    .param("expiryMonth", EXPIRY_MONTH.toString())
                    .param("expiryYear", ""+ EXPIRY_YEAR)
                )
                .andExpect(status().isOk())
                .andExpect(view().name("forward:details"))
                .andExpect(model().attributeDoesNotExist("errors"));
    }

    @Test
    public void shouldFailToAddBankCardDetails() throws Exception
    {
        mockMvc.perform(post("/add")
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .param("bankName", BANK_NAME)
                    .param("cardNumber", "123")
                    .param("expiryMonth", EXPIRY_MONTH.toString())
                    .param("expiryYear", ""+ EXPIRY_YEAR)
                )
                .andExpect(status().isOk())
                .andExpect(view().name("forward:details"))
                .andExpect(model().attributeExists("errors"));
    }

    @Test
    public void shouldFailToUploadEmptyFile() throws Exception
    {
        MockMultipartFile emptyFile = new MockMultipartFile("file", "empty.csv", "text/plain", "".getBytes());

        mockMvc.perform(MockMvcRequestBuilders.multipart("/upload")
                        .file(emptyFile)
                )
                .andExpect(status().isOk())
                .andExpect(view().name("forward:details"))
                .andExpect(model().attributeExists("errors"));
    }

    @Test
    public void shouldLoadBankCardFromFile() throws Exception
    {
        MockMultipartFile cardFile = new MockMultipartFile("file", "empty.csv", "text/plain",
                "HBOS,1234-5678-9012-3456,Jan-2019".getBytes());

        mockMvc.perform(MockMvcRequestBuilders.multipart("/upload")
                .file(cardFile)
        )
                .andExpect(status().isOk())
                .andExpect(view().name("forward:details"))
                .andExpect(model().attributeDoesNotExist("errors"));
    }

}